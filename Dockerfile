FROM node:12-alpine as builder

WORKDIR /usr/src/app

COPY . .

RUN npm install

RUN npm run build

FROM nginx:alpine

WORKDIR /usr/share/nginx/html

COPY --from=builder /usr/src/app/nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /usr/src/app/dist ./

CMD ["nginx", "-g", "daemon off;"]